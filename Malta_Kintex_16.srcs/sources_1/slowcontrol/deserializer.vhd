library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.package_constants.all;

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

entity deserializer is
port(
    i_clock        : in std_logic;
    o_dataParallel : out std_logic_vector(NBITS-1 downto 0);
    i_dataSerial   : in std_logic;    
    o_busy         : out std_logic
);
end deserializer;

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

architecture ARCH1_Deserializer of deserializer is

    --------
    -- wires
    --------
    
    signal m_i_clock        : std_logic := '0';
    signal m_o_busy         : std_logic := '0';
    signal m_i_dataSerial   : std_logic := '1';
    signal m_o_dataParallel : std_logic_vector(NBITS-1 downto 0); -- := X"0000";
    
    -----------------------
    -- counter: 16 bit word
    -----------------------
    
    signal m_counter_bitsInWord : natural range 0 to NBITS := 0;
    
begin

    ----------
    -- process
    ----------

    process_deserializer : process (i_clock) is
    begin
        if rising_edge(i_clock) then

            if m_o_busy = '0' then -- proceed only if not busy
                if m_i_dataSerial = '0' then -- if acknowledgement bit '0' is received, start transmission
                    m_o_busy <= '1';
                end if;
            else 
                if m_counter_bitsInWord = NBITS then -- last clock cycle used to unset busy and reset bit counter
                    m_o_busy <= '0';
                    m_counter_bitsInWord <= 0;
                else                
                    m_o_dataParallel(m_counter_bitsInWord) <= m_i_dataSerial;
                    m_counter_bitsInWord <= m_counter_bitsInWord + 1;
                end if;                           
            end if;
            
        end if;
    end process process_deserializer;

    ---------
    -- inputs
    ---------

    m_i_clock      <= i_clock;    
    m_i_dataSerial <= i_dataSerial;

    ----------
    -- outputs
    ----------
    
    o_busy         <= m_o_busy;
    o_dataParallel <= m_o_dataParallel;  

end ARCH1_Deserializer;
