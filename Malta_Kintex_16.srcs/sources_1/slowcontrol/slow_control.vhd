----------------------------------------------------------
-- MALTA Slow Control Transmitter and Receiver
-- Read write 16-bit words from MALTA using a 
-- Serializer and a Deserializer, that sends LSB first
-- Enrico.J.Schioppa@cern.ch
-- November 2017
-----------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library unisim;
use unisim.vcomponents.all;
library unimacro;
use unimacro.vcomponents.all;
use work.ipbus.all;
use work.package_constants.all;

entity slow_control is
  
  port (
    -- Clocks
    signal i_clk10 : in std_logic;
    
    -- FIFO Read
    signal i_fifoR_clk32 : in std_logic; 
    signal i_fifoR_rden  : in std_logic;
    signal o_fifoR_full  : out std_logic; 
    signal o_fifoR_empty : out std_logic;
    signal o_fifoR_out   : out std_logic_vector(NBITSIPBUS-1 downto 0);
    
    -- FIFO Write
    signal i_fifoW_clk32 : in std_logic; 
    signal i_fifoW_wren  : in std_logic;
    signal i_fifoW_in    : in std_logic_vector(NBITSIPBUS-1 downto 0);
    signal o_fifoW_full  : out std_logic;
    signal o_fifoW_empty : out std_logic;
    
    -- Serial I/O
    signal i_fromMALTA   : in std_logic;  -- We should receive what we have sent from o_toMALTA
    signal o_toMALTA     : out std_logic; -- This is the 4322 "chorizo" that contains the configuration
    
    -- Reset
    signal i_reset       : in std_logic;  -- remove semicolon after debugging
    
    -- Load  17/03/2021 Added Leyre so review me!!!
    --signal i_load        : in std_logic;       
    
    
    -- temporary debug outputs
    signal o_fifoW_rden      : out std_logic;
    signal o_serializer_start: out std_logic;
    signal o_serializer_busy : out std_logic       
  );

end slow_control;


architecture rtl of slow_control is
  
    -- FIFOW          
    signal m_fifoW_reset       : std_logic := '0';
    signal m_fifoW_empty       : std_logic := '0';
    signal m_fifoW_almostEmpty : std_logic := '1';
    signal m_fifoW_almostFull  : std_logic := '1';
    signal m_fifoW_full        : std_logic := '1';
    signal m_fifoW_rdCount     : std_logic_vector(11-1 downto 0);
    signal m_fifoW_rdErr       : std_logic := '0';
    signal m_fifoW_fromUser    : std_logic_vector(NBITS-1 downto 0) := NULLWORD;
    signal m_fifoW_rden        : std_logic := '0';
    signal m_fifoW_toSerializer: std_logic_vector(NBITS-1 downto 0) := NULLWORD;
    signal m_fifoW_wren        : std_logic := '0';
    signal n_fifoW_CNT         : natural range 0 to 2; -- aux

    -- serializer
    signal m_serializer_start  : std_logic := '0';
    signal m_serializer_busy   : std_logic := '0';
    signal m_serializer_toMALTA: std_logic := '1';

    -- FIFOR          
    signal m_fifoR_reset       : std_logic := '0';
    signal m_fifoR_empty       : std_logic := '0';
    signal m_fifoR_almostEmpty : std_logic := '1';
    signal m_fifoR_almostFull  : std_logic := '1';
    signal m_fifoR_full        : std_logic := '1';
    signal m_fifoR_rdCount     : std_logic_vector(11-1 downto 0);
    signal m_fifoR_rdErr       : std_logic := '0';
    signal m_fifoR_fromDeserializer: std_logic_vector(NBITS-1 downto 0) := NULLWORD;
    signal m_fifoR_rden        : std_logic := '0';
    signal m_fifoR_toUser      : std_logic_vector(NBITS-1 downto 0) := NULLWORD;
    signal m_fifoR_wren        : std_logic := '0';
    signal n_fifoR_CNT         : natural range 0 to 2; -- aux    

    -- deserializer
    signal m_deserializer_busy     : std_logic := '0';
    signal m_deserializer_fromMALTA: std_logic := '1';
    
    -- FIFOW
    component fifo_small is 
        port (
            rst           : IN STD_LOGIC;
            wr_clk        : IN STD_LOGIC;
            rd_clk        : IN STD_LOGIC;
            din           : IN STD_LOGIC_VECTOR(NBITS-1 DOWNTO 0);
            wr_en         : IN STD_LOGIC;
            rd_en         : IN STD_LOGIC;
            dout          : OUT STD_LOGIC_VECTOR(NBITS-1 DOWNTO 0);
            full          : OUT STD_LOGIC;
            almost_full   : OUT STD_LOGIC;
            empty         : OUT STD_LOGIC
        );
    end component fifo_small;    
    
begin
        
     -- wiring   
     m_fifoW_fromUser         <= i_fifoW_in(NBITSIPBUS-NBITS-1 downto 0);  
     o_fifoR_out              <= ("0000000000000000" & m_fifoR_toUser);
     m_fifoR_reset            <= i_reset;
     m_fifoW_reset            <= i_reset;  
     m_fifoR_rden             <= i_fifoR_rden;
     m_fifoW_wren             <= i_fifoW_wren;
     o_fifoR_empty            <= m_fifoR_empty;
     o_fifoW_empty            <= m_fifoW_empty;
     o_fifoR_full             <= m_fifoR_full;
     o_fifoW_full             <= m_fifoW_full;
     m_deserializer_fromMALTA <= i_fromMALTA;
     o_toMALTA                <= m_serializer_toMALTA;
        
     -- temporary debug wires
     o_fifoW_rden             <= m_fifoW_rden;
     o_serializer_start       <= m_serializer_start;
     o_serializer_busy        <= m_serializer_busy;   
        
    ---------------------------------------------------------------------
    -- TRANSMISSION -----------------------------------------------------
    ---------------------------------------------------------------------

    -- FIFOW
    FIFOW : fifo_small
        port map(
            rst           => m_fifoW_reset,
            wr_clk        => i_fifoW_clk32,
            rd_clk        => i_clk10,
            din           => m_fifoW_fromUser,
            wr_en         => m_fifoW_wren,  
            rd_en         => m_fifoW_rden,
            dout          => m_fifoW_toSerializer,
            full          => m_fifoW_full,
            almost_full   => m_fifoW_almostFull,
            empty         => m_fifoW_empty
        );
         
     -- serializer
     portMap_serializer : entity work.serializer
         port map(
             i_clock        => i_clk10,
             i_dataParallel => m_fifoW_toSerializer,
             o_dataSerial   => m_serializer_toMALTA,
             i_start        => m_serializer_start,
             o_busy         => m_serializer_busy
         );
 
    -- writing from FIFOW to serializer
    process_tx : process (i_clk10) is
        begin
            if rising_edge(i_clk10) then            
                                                                                             
                 if m_serializer_busy = '0' then -- if tx not busy ...
                     if m_serializer_start = '0' then
                         if m_fifoW_empty = '0' then -- if FIFO not empty
                             if m_fifoW_rden = '0' then -- one clock cycle to read from FIFO
                                 m_fifoW_rden <= '1';
                             else
                                 m_fifoW_rden <= '0';
                                 m_serializer_start <= '1';
                             end if;
                         end if;
                     else
                         m_serializer_start <= '0';     
                     end if;
                 end if;
                   
            end if; -- closes rising edge            
        end process process_tx;       

    ---------------------------------------------------------------------
    -- RECEPTION --------------------------------------------------------
    ---------------------------------------------------------------------

    -- FIFOR
    FIFOR : fifo_small
        port map(
            rst           => m_fifoR_reset,
            wr_clk        => i_clk10,
            rd_clk        => i_fifoR_clk32,
            din           => m_fifoR_fromDeserializer,
            wr_en         => m_fifoR_wren,  
            rd_en         => m_fifoR_rden,
            dout          => m_fifoR_toUser,
            full          => m_fifoR_full,
            almost_full   => m_fifoR_almostFull,
            empty         => m_fifoR_empty
        );
 
     -- deserializer
     portMap_deserializer : entity work.deserializer
       port map(
           i_clock        => i_clk10,
           o_dataParallel => m_fifoR_fromDeserializer,
           i_dataSerial   => m_deserializer_fromMALTA,
           o_busy         => m_deserializer_busy
       );   
        
    -- writing from deserializer to FIFOR
    process_rx : process(i_clk10) is     
    begin
        if rising_edge(i_clk10) then
            if n_fifoR_CNT = 0 then
                if m_deserializer_busy = '1' then                
                    n_fifoR_CNT <= 1;
                end if;
            elsif n_fifoR_CNT = 1 then
                if m_deserializer_busy = '0' then
                    m_fifoR_wren <= '1';
                    n_fifoR_CNT <= 2;
                end if;
            else
                m_fifoR_wren <= '0';
                n_fifoR_CNT <= 0;                          
            end if;                       
        end if;
    end process process_rx;      
	
end rtl;