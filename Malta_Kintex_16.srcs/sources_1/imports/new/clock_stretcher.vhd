library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity ClkStretcher is
 generic(
  multiplier   : natural := 10000
 ); 
 Port (
  i_clk : in std_logic;
  o_sig : out std_logic
 );
end ClkStretcher;

----------------------------------------------------------------------------------------------------------------------------------------
architecture Main of ClkStretcher is

 signal m_count : natural   range 0 to multiplier := 0;
 signal m_sig   : std_logic := '0';
 
begin

 process (i_clk) is 
  begin  
  if rising_edge(i_clk) then
   if m_count = multiplier then
    m_sig <= not m_sig;
    m_count <= 0;
   else 
    m_count <= m_count+1;
   end if;
  end if;    
 end process;
 
 o_sig <= m_sig;
 
end Main;
