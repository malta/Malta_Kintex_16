----------------------------------------------------------------------------------
-- Engineer: you wish
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity L1AClipper is
Port ( 
  i_clk_fast    : in STD_LOGIC;
  i_clk_slow    : in STD_LOGIC;
  i_sig         : in STD_LOGIC;
  o_sig         : out STD_LOGIC;
  o_debug1      : out STD_LOGIC;
  o_debug2      : out STD_LOGIC
);
end L1AClipper;

architecture picoTDC of L1AClipper is
  signal m_counter_fast : natural range 0 to 31 :=0; --was 33 --was 9
  signal m_counter_slow : natural range 0 to 31 :=0; --was 33 --was 9
  signal m_counter_slow2: natural range 0 to 31 :=0; --was 33 --was 9
  signal m_latched      : std_logic := '0';
  signal m_locked       : std_logic := '0';
  signal m_sig          : std_logic := '0';
  
begin

  o_sig <= m_sig;
  
  latch: process(i_clk_fast) is begin
  if rising_edge(i_clk_fast) then
    -- if trigger is latched, set flag accordinglz
    --if i_sig = '1' and m_latched = '0' and m_counter_fast = 0 then
   if i_sig = '1' and m_counter_fast = 0 then
        m_counter_fast <= m_counter_fast+1;
        m_latched <= '1';
   elsif m_counter_fast > 0 and m_counter_fast < 16 then
        m_counter_fast <= m_counter_fast+1;
        --m_latched <= '1';
   elsif m_counter_fast > 15 then          -- stretch latched signal to 25ns
        m_counter_fast <= 0;--m_counter_fast+1;
        m_latched <= '0';
   else
        m_latched <= '0';
   end if;    
  end if;  
  end process latch;
  
  
--  pipeOut: process(i_clk_slow) is begin
--  if rising_edge(i_clk_slow) then
--    if (m_latched = '1' and m_locked = '0') or m_counter_slow > 0 then
--        m_locked <= '1';
--        if m_counter_slow = 8 then -- use this  to delay the signal to the picoTDC
--            m_counter_slow <= m_counter_slow+1;
--            m_sig <= '1';
--        elsif m_counter_slow < 8 then
--            m_counter_slow <= m_counter_slow+1;
--            m_sig <= '0';
--        elsif m_counter_slow > 8 and m_counter_slow < 16 then
--            m_counter_slow <= m_counter_slow+1;
--            m_sig <= '0';
--        elsif m_counter_slow > 15 then  -- set a dead time with this
--            m_counter_slow <= 0;
--            m_sig <= '0';
--            m_locked <= '0';
--        end if;
--    else
--        m_sig <= '0';
--    end if;
--  end if;  
--  end process pipeOut;
  
  pipeOut: process(i_clk_slow) is begin
  if rising_edge(i_clk_slow) then
   if m_latched = '1' and m_counter_slow=0 then
     m_locked <= '1';
     m_counter_slow <= m_counter_slow+1;
   elsif m_counter_slow=1 then
     m_locked <= '0';
     m_counter_slow <= 0; 
   else
     m_locked <= '0';
   end if;
  end if;  
  end process pipeOut;
  
  
  
  delay: process(i_clk_slow) is begin
    if rising_edge(i_clk_slow) then
      if m_locked = '1' and m_counter_slow2=0 then
         m_counter_slow2 <= 1;
         m_sig <= '0';
      elsif m_counter_slow2 > 0 and m_counter_slow2 < 9 then
         m_counter_slow2 <= m_counter_slow2+1;
      elsif m_counter_slow2 = 9 then
         m_sig <= '1'; 
         m_counter_slow2 <= m_counter_slow2+1;
      elsif m_counter_slow2 = 10 then
         m_sig <= '0'; 
         m_counter_slow2 <= m_counter_slow2+1;
      elsif m_counter_slow2 > 10 then
         m_sig <= '0'; 
         m_counter_slow2 <= 0;
      else
         m_sig <= '0';
      end if;
 
    end if;  
  end process delay;
  
  o_debug1 <= m_latched;
  o_debug2 <= m_locked;

end architecture picoTDC;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
architecture maltaAO of L1AClipper is
  signal m_counter : natural range 0 to 162 :=0; --was 33 --was 9
  signal m_sig        : std_logic := '0';
  signal m_AnotherSig : std_logic := '0';
    
begin
    
  o_sig <= m_sig;
    
  process (i_clk_fast) is begin 
    if rising_edge(i_clk_fast) then
      m_AnotherSig  <= i_sig;
      
      if m_counter=0 then
        if m_AnotherSig='1' then
          m_counter <= m_counter+1;
          m_sig<='1';
        else 
          m_sig<='0';
        end if;
      elsif m_counter=1 then
        m_sig<='0';
        m_counter <= m_counter+1;
      elsif m_counter=2 then
        m_sig<='0';
        m_counter <= m_counter+1;
      elsif m_counter=3 then
        m_sig<='0';
        m_counter <= m_counter+1;
      else 
        m_counter <= m_counter+1;
      end if; 
    end if; 
  end process;
    
end maltaAO;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

architecture maltaAOdebug of L1AClipper is
  signal m_counter : natural range 0 to 162 :=0; --was 33 --was 9
  signal m_sig        : std_logic := '0';
  signal m_sig_not_not        : std_logic := '0';

  signal m_AnotherSig : std_logic := '0';
    
begin
  m_sig_not_not<=NOT(NOT(m_sig));
  o_sig <= m_sig_not_not;
    
  process (i_clk_fast) is begin 
    if rising_edge(i_clk_fast) then
      m_AnotherSig  <= i_sig;
      if m_counter =0 then
        if m_AnotherSig='1' then
          m_counter <= m_counter+1;
          m_sig<='1';
        else 
          m_sig<='0';
          m_counter<=0;
        end if;
      elsif  m_counter > 20 then
        m_counter<=0;
        m_sig<='0';
      else 
        m_sig<='1';
        m_counter <= m_counter+1;
      end if;
    end if; 
  end process;
    
end maltaAOdebug;
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
architecture picoTDCold of L1AClipper is

  signal m_counter                  : integer range 0 to 250    :=0; --was 33 --was 9
  signal m_lock                     : std_logic                 := '0';
    
begin
    
    process (i_clk_fast) is begin 
    if (rising_edge(i_clk_fast)) then
        -- only clip a signal once
        -- this means the clipper must not be locked and there must be a signal on the output
        -- once the clipping has started, the process will keep o_sig high for 8 clk cycles
        -- the clipper is locked once a signal is detected and stays locked as long as it is clipping and until i_sig goes low again
        if (m_lock = '0' and i_sig = '1') or m_counter > 0 then
            m_lock              <= '1';
            -- choose the time window where the clipped bit is put out by changing the indices of the 8 lines in the case structure
            -- start at 64 for a 64*(25/8) ns delay
            -- 32 -> 100ns
            -- 192 -> 600ns delay
            m_counter <= m_counter +1;
            case m_counter is
                when 192 =>           o_sig <= '1';
                when 193 =>           o_sig <= '1';
                when 194 =>           o_sig <= '1';
                when 195 =>           o_sig <= '1';
                when 196 =>           o_sig <= '1';
                when 197 =>           o_sig <= '1';
                when 198 =>           o_sig <= '1';
                when 199 =>           o_sig <= '1';
                when 200 =>           o_sig <= '1'; --new from VD
                when 250 =>
                                      o_sig <= '0';
                                      m_counter <= 0;
                when others =>        o_sig <= '0';
            end case;
        else
            -- stop clipped L1A
            o_sig               <= '0';
            -- when i_sig goes low, unlock clipper to accept new L1A
            if i_sig = '0' then
                m_lock          <= '0';
            end if;
        end if;        
    end if; 
    end process;
    
end picoTDCold;

