----------------------------------------------
-- Oversampling Module
--
-- Valerio.Dao@cern.ch
-- LLuis.Simon.Argemi@cern.ch
-- Fernando.Carrio.Argos@cern.ch
-- Carlos.Solans@cern.ch
-- 12 December 2017
----------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;


entity oversampling_module is
 Generic (
  N_TAP_0 : natural range 0 to 31 := 2;
  N_TAP_5 : natural range 0 to 31 := 0
 );
  Port (
  -- control
  i_control :          in std_logic_vector (31 downto 0) := (others => '0');
  o_status :           out std_logic_vector (31 downto 0) := (others => '0');
  
  -- clocks
  i_clock_200MHz :     in std_logic;
  i_clock_500MHz :     in std_logic;
  i_clock_500MHz_90 :  in std_logic;
  i_clock_500MHz_180 : in std_logic;
  i_clock_500MHz_270 : in std_logic;
     
  --MALTA FMC inputs
  i_FMC_P :            in std_logic;
  i_FMC_N :            in std_logic;
     
  -- individual oversampled signals
  o_sampled_bits:      out std_logic_vector (7 downto 0);
  -- OR of oversampled signals
  o_sigOR       :      out std_logic;
     
  --signals after IDELAYE2 (for debugging)
  o_delayed0:          out std_logic;
  o_delayed5:          out std_logic
 );
end oversampling_module;

architecture diff_in of oversampling_module is

 signal m_FMC_P   , m_FMC_N    : std_logic;
 signal m_idelIn_0 : std_logic;
 signal m_idelIn_5 : std_logic;
 signal m_IDELAY_0, m_IDELAY_5 : std_logic;
  
 signal m_OR_0 : std_logic := '0';
 signal m_OR_5 : std_logic := '0';
 signal m_tmpOR_0, m_tmpOR_1, m_tmpOR_2, m_tmpOR_3, m_tmpOR_4, m_tmpOR_5 : std_logic := '0';
   
 signal m_sampled_bits : std_logic_vector (7 downto 0);
 signal m_tmpBits      : std_logic_vector (7 downto 0);
 signal m_tmpBits_0, m_tmpBits_1, m_tmpBits_2, m_tmpBits_3, m_tmpBits_4, m_tmpBits_5 : std_logic_vector (7 downto 0) := (others => '0');
    
 attribute ASYNC_REG : string;
 attribute ASYNC_REG of m_tmpOR_0: signal is "TRUE";
 attribute ASYNC_REG of m_tmpOR_1: signal is "TRUE";
 attribute ASYNC_REG of m_tmpOR_2: signal is "TRUE";
 attribute ASYNC_REG of m_tmpOR_3: signal is "TRUE";
 attribute ASYNC_REG of m_tmpOR_4: signal is "TRUE";
 attribute ASYNC_REG of m_tmpOR_5: signal is "TRUE";
 attribute ASYNC_REG of m_tmpBits_0: signal is "TRUE";
 attribute ASYNC_REG of m_tmpBits_1: signal is "TRUE";
 attribute ASYNC_REG of m_tmpBits_2: signal is "TRUE";
 attribute ASYNC_REG of m_tmpBits_3: signal is "TRUE";
 attribute ASYNC_REG of m_tmpBits_4: signal is "TRUE";
 attribute ASYNC_REG of m_tmpBits_5: signal is "TRUE";
 
begin

 IBUFDS_DIFF_OUT_inst: IBUFDS_DIFF_OUT
 generic map( DIFF_TERM=>TRUE, IBUF_LOW_PWR=>TRUE, IOSTANDARD=>"BLVDS_25" ) --DEFAULT
 port map(
  O=>m_FMC_P , I =>i_FMC_P,
  OB=>m_FMC_N, IB=>i_FMC_N
 );
 
 ----configurarion for Malta
 m_idelIn_0 <= not m_FMC_P;
 m_idelIn_5 <= m_FMC_N;
 
-- ----configuratrion for emulator  
---- m_idelIn_0 <= m_FMC_P;
---- m_idelIn_5 <= not m_FMC_N;
 
 Inst_IDELAY_CFG_P: entity work.IDELAY_CFG
 generic map(k_delay =>"00000")
 port map(
     i_clk200 => i_clock_200MHz,
     i_reset => '0',
     i_enable => i_control(31),
     i_data => m_idelIn_0,--FMC_P_not,--m_FMC_P,
     i_count => i_control(4 downto 0),
     o_data_dly => m_IDELAY_0,
     o_countRBK => o_status(4 downto 0));
 
 Inst_IDELAY_CFG_N: entity work.IDELAY_CFG
 generic map(k_delay =>"00011")
 port map(
     i_clk200 => i_clock_200MHz,
     i_reset => '0',
     i_enable => i_control(31),
     i_data => m_idelIn_5,
     i_count => i_control(9 downto 5),
     o_data_dly => m_IDELAY_5,
     o_countRBK => o_status(9 downto 5));
 
 -- o_delayed0 <= m_idelIn_0;
--  o_delayed0 <= not m_FMC_P;
--  o_delayed5 <= m_FMC_N;
  o_delayed0 <= m_IDELAY_0;
  o_delayed5 <= m_IDELAY_5;  
  o_status(31 downto 10) <= (others => '0');
 
   
 serdes_0 : ISERDESE2
 generic map (
  INTERFACE_TYPE=>"OVERSAMPLE",
  SERDES_MODE   => "MASTER",
  DATA_RATE=>"DDR", DATA_WIDTH=>4, OFB_USED=>"FALSE", NUM_CE=>1,
  IOBDELAY=>"BOTH", DYN_CLKDIV_INV_EN=>"FALSE", DYN_CLK_INV_EN=>"FALSE",
  INIT_Q1=>'0', INIT_Q2=>'0', INIT_Q3=>'0', INIT_Q4=>'0',
  SRVAL_Q1=>'0', SRVAL_Q2=>'0', SRVAL_Q3=>'0', SRVAL_Q4=>'0')
 port map (
  CLK=>i_clock_500MHz, CLKB=>i_clock_500MHz_180, OCLK=>i_clock_500MHz_90, OCLKB=>i_clock_500MHz_270,
  D=>'0', BITSLIP=>'0', CE1=>'1', CE2=>'1', CLKDIV=>'0', CLKDIVP=>'0',
  DYNCLKDIVSEL=>'0', DYNCLKSEL=>'0', OFB=>'0', RST=>'0', SHIFTIN1=>'0', SHIFTIN2=> '0',
  DDLY            => m_IDELAY_0,
  O               => open,
  Q1              => m_sampled_bits(0),
  Q2              => m_sampled_bits(4),
  Q3              => m_sampled_bits(2),
  Q4              => m_sampled_bits(6),
  Q5=>open, Q6=>open, Q7=>open, Q8=>open,
  SHIFTOUT1=>open, SHIFTOUT2=>open
 );
                 
 serdes_5 : ISERDESE2
 generic map (
  INTERFACE_TYPE=>"OVERSAMPLE",
  SERDES_MODE   => "MASTER",
  DATA_RATE=>"DDR", DATA_WIDTH=>4, OFB_USED=>"FALSE", NUM_CE=>1,
  IOBDELAY=>"BOTH", DYN_CLKDIV_INV_EN=>"FALSE", DYN_CLK_INV_EN=>"FALSE",
  INIT_Q1=>'0', INIT_Q2=>'0', INIT_Q3=>'0', INIT_Q4=>'0',
  SRVAL_Q1=>'0', SRVAL_Q2=>'0', SRVAL_Q3=>'0', SRVAL_Q4=>'0')
 port map (
  CLK=>i_clock_500MHz, CLKB=>i_clock_500MHz_180, OCLK=>i_clock_500MHz_90, OCLKB=>i_clock_500MHz_270,
  D=>'0', BITSLIP=>'0', CE1=>'1', CE2=>'1', CLKDIV=>'0', CLKDIVP=>'0',
  DYNCLKDIVSEL=>'0', DYNCLKSEL=>'0', OFB=>'0', RST=>'0', SHIFTIN1=>'0', SHIFTIN2=> '0',
  DDLY            => m_IDELAY_5,
  O               => open,
  Q1              => m_sampled_bits(1),
  Q2              => m_sampled_bits(5),
  Q3              => m_sampled_bits(3),
  Q4              => m_sampled_bits(7),
  Q5=>open, Q6=>open, Q7=>open, Q8=>open,
  SHIFTOUT1=>open, SHIFTOUT2=>open
 );

                         
 --perfomrming the OR in 2 steps and carrying around the vector results
 --adding 1-2 extra FF for flexibility (??)
 process(i_clock_500MHz)
 begin
  if rising_edge(i_clock_500MHz) then  
   m_OR_0 <= ( m_sampled_bits(0) or  m_sampled_bits(2) or  m_sampled_bits(4)  or  m_sampled_bits(6) );
   m_OR_5 <= ( m_sampled_bits(1) or  m_sampled_bits(3) or  m_sampled_bits(5)  or  m_sampled_bits(7) );
   m_tmpOR_0 <= m_OR_0 or m_OR_5;
   m_tmpOR_1 <= m_tmpOR_0;
   m_tmpOR_2 <= m_tmpOR_1;
   m_tmpOR_3 <= m_tmpOR_2;
--   m_tmpOR_4 <= m_tmpOR_3;
--   m_tmpOR_5 <= m_tmpOR_4;
   o_sigOR   <= m_tmpOR_3;
     
   m_tmpBits      <= m_sampled_bits;
   m_tmpBits_0    <= m_tmpBits;
   m_tmpBits_1    <= m_tmpBits_0;
   m_tmpBits_2    <= m_tmpBits_1;
   m_tmpBits_3    <= m_tmpBits_2;
--   m_tmpBits_4    <= m_tmpBits_3;
--   m_tmpBits_5    <= m_tmpBits_4;
   o_sampled_bits <= m_tmpBits_3;
  end if;
 end process;  
   
end diff_in;









--------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------
---- n is ignored in this mode

--architecture single_in of oversampling_module is

-- signal m_FMC_P   , m_FMC_N    : std_logic;
-- signal m_FMC_N_not : std_logic;
 
-- signal m_IDELAY_0, m_IDELAY_5 : std_logic;
  
-- signal m_OR_0 : std_logic := '0';
-- signal m_OR_5 : std_logic := '0';
-- signal m_tmpOR_0, m_tmpOR_1, m_tmpOR_2 : std_logic := '0';
   
-- signal m_sampled_bits : std_logic_vector (7 downto 0);
-- signal m_tmpBits      : std_logic_vector (7 downto 0);
-- signal m_tmpBits_0, m_tmpBits_1, m_tmpBits_2 : std_logic_vector (7 downto 0);
    
--begin

---- IBUFDS_DIFF_OUT_inst: IBUFDS_DIFF_OUT
---- generic map( DIFF_TERM=>TRUE, IBUF_LOW_PWR=>TRUE, IOSTANDARD=>"DEFAULT" )
---- port map(
----  O=>m_FMC_P, I=>i_FMC_P,
----  OB=>m_FMC_N, IB=>i_FMC_N
---- );
---- m_FMC_N_not <= not m_FMC_N;
-- m_FMC_P <= i_FMC_P;
-- m_FMC_N <= i_FMC_P;       
        
-- Idelay_0tap : IDELAYE2 
-- generic map (
--  IDELAY_VALUE            => N_TAP_0, 
--  REFCLK_FREQUENCY        => 200.0,
--  CINVCTRL_SEL=>"FALSE", DELAY_SRC=>"DATAIN", IDELAY_TYPE=> "FIXED", HIGH_PERFORMANCE_MODE=>"TRUE", PIPE_SEL=>"FALSE", SIGNAL_PATTERN=>"DATA")
-- port map (
--  LD=>'0', LDPIPEEN=>'0', REGRST=>'0', INC=>'0', CNTVALUEOUT=>open, C=>'0', CE=>'0', CINVCTRL=>'0', CNTVALUEIN=>"00000",
--  IDATAIN => '0',
--  DATAIN  => m_FMC_P,
--  DATAOUT => m_IDELAY_0        
-- ); 
        
-- Idelay_5tap : IDELAYE2 
-- generic map (
--  IDELAY_VALUE            => N_TAP_5, 
--  REFCLK_FREQUENCY        => 200.0, 
--  CINVCTRL_SEL=>"FALSE", DELAY_SRC=>"DATAIN", IDELAY_TYPE=> "FIXED", HIGH_PERFORMANCE_MODE=>"TRUE", PIPE_SEL=>"FALSE", SIGNAL_PATTERN=>"DATA")
-- port map (
--  LD=>'0', LDPIPEEN=>'0', REGRST=>'0', INC=>'0', CNTVALUEOUT=>open, C=>'0', CE=>'0', CINVCTRL=>'0', CNTVALUEIN=>"00000",
--  IDATAIN => '0',
--  DATAIN  => m_FMC_P,
--  DATAOUT => m_IDELAY_5
-- );
---- o_delayed0 <= m_IDELAY_0;
---- o_delayed5 <= m_IDELAY_5; 
 
   
-- serdes_0 : ISERDESE2
-- generic map (
--  INTERFACE_TYPE=>"OVERSAMPLE",
--  SERDES_MODE   => "MASTER",
--  DATA_RATE=>"DDR", DATA_WIDTH=>4, OFB_USED=>"FALSE", NUM_CE=>1,
--  IOBDELAY=>"BOTH", DYN_CLKDIV_INV_EN=>"FALSE", DYN_CLK_INV_EN=>"FALSE",
--  INIT_Q1=>'0', INIT_Q2=>'0', INIT_Q3=>'0', INIT_Q4=>'0',
--  SRVAL_Q1=>'0', SRVAL_Q2=>'0', SRVAL_Q3=>'0', SRVAL_Q4=>'0')
-- port map (
--  CLK=>i_clock_500MHz, CLKB=>i_clock_500MHz_180, OCLK=>i_clock_500MHz_90, OCLKB=>i_clock_500MHz_270,
--  D=>'0', BITSLIP=>'0', CE1=>'1', CE2=>'1', CLKDIV=>'0', CLKDIVP=>'0',
--  DYNCLKDIVSEL=>'0', DYNCLKSEL=>'0', OFB=>'0', RST=>'0', SHIFTIN1=>'0', SHIFTIN2=> '0',
--  DDLY            => m_IDELAY_0,
--  O               => open,
--  Q1              => m_sampled_bits(0),
--  Q2              => m_sampled_bits(4),
--  Q3              => m_sampled_bits(2),
--  Q4              => m_sampled_bits(6),
--  Q5=>open, Q6=>open, Q7=>open, Q8=>open,
--  SHIFTOUT1=>open, SHIFTOUT2=>open
-- );
                 
-- serdes_5 : ISERDESE2
-- generic map (
--  INTERFACE_TYPE=>"OVERSAMPLE",
--  SERDES_MODE   => "MASTER",
--  DATA_RATE=>"DDR", DATA_WIDTH=>4, OFB_USED=>"FALSE", NUM_CE=>1,
--  IOBDELAY=>"BOTH", DYN_CLKDIV_INV_EN=>"FALSE", DYN_CLK_INV_EN=>"FALSE",
--  INIT_Q1=>'0', INIT_Q2=>'0', INIT_Q3=>'0', INIT_Q4=>'0',
--  SRVAL_Q1=>'0', SRVAL_Q2=>'0', SRVAL_Q3=>'0', SRVAL_Q4=>'0')
-- port map (
--  CLK=>i_clock_500MHz, CLKB=>i_clock_500MHz_180, OCLK=>i_clock_500MHz_90, OCLKB=>i_clock_500MHz_270,
--  D=>'0', BITSLIP=>'0', CE1=>'1', CE2=>'1', CLKDIV=>'0', CLKDIVP=>'0',
--  DYNCLKDIVSEL=>'0', DYNCLKSEL=>'0', OFB=>'0', RST=>'0', SHIFTIN1=>'0', SHIFTIN2=> '0',
--  DDLY            => m_IDELAY_5,
--  O               => open,
--  Q1              => m_sampled_bits(1),
--  Q2              => m_sampled_bits(5),
--  Q3              => m_sampled_bits(3),
--  Q4              => m_sampled_bits(7),
--  Q5=>open, Q6=>open, Q7=>open, Q8=>open,
--  SHIFTOUT1=>open, SHIFTOUT2=>open
-- );

                         
-- --perfomrming the OR in 2 steps and carrying around the vector results
-- --adding 1-2 extra latches for flexibility (??)
-- process(i_clock_500MHz)
-- begin
--  if rising_edge(i_clock_500MHz) then  
--   m_OR_0 <= ( m_sampled_bits(0) or  m_sampled_bits(2) or  m_sampled_bits(4)  or  m_sampled_bits(6) );
--   m_OR_5 <= ( m_sampled_bits(1) or  m_sampled_bits(3) or  m_sampled_bits(5)  or  m_sampled_bits(7) );
--   m_tmpOR_0 <= m_OR_0 or m_OR_5;
--   --m_tmpOR_1 <= m_tmpOR_0;
--   o_sigOR   <= m_tmpOR_0;
     
--   m_tmpBits      <= m_sampled_bits;
--   m_tmpBits_0    <= m_tmpBits;
--   --m_tmpBits_1    <= m_tmpBits_0;
--   o_sampled_bits <= m_tmpBits_0;
--  end if;
-- end process;  
   
--end single_in;