-- Contains the instantiation of the Xilinx MAC & 1000baseX pcs/pma & GTP transceiver cores
--
-- Do not change signal names in here without correspondig alteration to the timing contraints file
--
-- Dave Newbold, April 2011
-- Changes made by Carlos, November 2017
--
-- $Id$

library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

entity eth_7s_1000basex is
	port(
		gt_clkp: in std_logic;
		gt_clkn: in std_logic;
		gt_txp: out std_logic;
		gt_txn: out std_logic;
		gt_rxp: in std_logic;
		gt_rxn: in std_logic;
		clk125_out: out std_logic;
		clk125_fr: out std_logic;
		rsti: in std_logic;
		locked: out std_logic;
		tx_data: in std_logic_vector(7 downto 0);
		tx_valid: in std_logic;
		tx_last: in std_logic;
		tx_error: in std_logic;
		tx_ready: out std_logic;
		rx_data: out std_logic_vector(7 downto 0);
		rx_valid: out std_logic;
		rx_last: out std_logic;
		rx_error: out std_logic;
		clk200: in std_logic
	);

end eth_7s_1000basex;

architecture rtl of eth_7s_1000basex is

  ------------------------------------------------------------------------------
  -- Declare the Tri-Mode Ethernet MAC
  ------------------------------------------------------------------------------
   component tri_mode_ethernet_mac_0
   port(
      -- Clock and speed
      -------------------
      glbl_rstn                  : in  std_logic;
      rx_axi_rstn                : in  std_logic;
      tx_axi_rstn                : in  std_logic;
      rx_reset                   : out std_logic;
      gtx_clk                    : in  std_logic; 
      rx_mac_aclk                : out std_logic;
      clk_enable                 : in  std_logic;
      speedis100                 : out std_logic;
      speedis10100               : out std_logic;
      -- Receiver interface
      ----------------------
      rx_axis_mac_tdata          : out std_logic_vector(7 downto 0);
      rx_axis_mac_tvalid         : out std_logic;
      rx_axis_mac_tlast          : out std_logic;
      rx_axis_mac_tuser          : out std_logic;
      --rx_axis_filter_tuser       : out std_logic_vector(4 downto 0);
      -- Receiver sideband pins
      --------------------------
      rx_statistics_vector       : out std_logic_vector(27 downto 0);
      rx_statistics_valid        : out std_logic;
      -- Transmitter interface
      -------------------------
      tx_axis_mac_tdata          : in  std_logic_vector(7 downto 0);
      tx_axis_mac_tvalid         : in  std_logic;
      tx_axis_mac_tlast          : in  std_logic;
      tx_axis_mac_tuser          : in  std_logic_vector(0 downto 0);
      tx_axis_mac_tready         : out std_logic;
      -- Transmitter sideband pins
      -----------------------------
      tx_ifg_delay               : in  std_logic_vector(7 downto 0);
      tx_statistics_vector       : out std_logic_vector(31 downto 0);
      tx_statistics_valid        : out std_logic;
      tx_mac_aclk                : out std_logic;
      tx_reset                   : out std_logic;
      -- Flow control interface
      --------------------------
      pause_req                  : in  std_logic;
      pause_val                  : in  std_logic_vector(15 downto 0);
      -- GMII interface
      ------------------
      gmii_txd                   : out std_logic_vector(7 downto 0);
      gmii_tx_en                 : out std_logic;
      gmii_tx_er                 : out std_logic;
      gmii_rxd                   : in  std_logic_vector(7 downto 0);
      gmii_rx_dv                 : in  std_logic;
      gmii_rx_er                 : in  std_logic;
      -- Configuration vector
      ------------------------
      rx_configuration_vector    : in  std_logic_vector(79 downto 0);
      tx_configuration_vector    : in  std_logic_vector(79 downto 0)
  
   );
   end component;

  ------------------------------------------------------------------------------
  -- Declare the Gigabit Ethernet PCS PMA
  ------------------------------------------------------------------------------

   component gig_ethernet_pcs_pma_0
        port(
        -- Transceiver Interface
        ------------------------
        gtrefclk               : in std_logic;                     -- Very high quality clock for GT transceiver
        gtrefclk_bufg          : in std_logic;                           
        txp                    : out std_logic;                    -- Differential +ve of serial transmission from PMA to PMD.
        txn                    : out std_logic;                    -- Differential -ve of serial transmission from PMA to PMD.
        rxp                    : in std_logic;                     -- Differential +ve for serial reception from PMD to PMA.
        rxn                    : in std_logic;                     -- Differential -ve for serial reception from PMD to PMA.
        txoutclk               : out std_logic;                    -- Recovered clock from GT TX (62.5MHz or 125MHz)
        rxoutclk               : out std_logic;                    -- Recovered clock from GT RX (62.5MHz or 125MHz)
        userclk                : in std_logic;                     -- 62.5 MHz global clock
        userclk2               : in std_logic;                     -- 125 MHz global clock
        rxuserclk              : in std_logic;                     -- 62.5 MHz buffered clock from rxoutclk
        rxuserclk2             : in std_logic;                     -- 125 MHz buffered clock from rxoutclk
        pma_reset              : in std_logic;                     -- transceiver PMA reset signal
        mmcm_locked            : in std_logic;                     -- Locked signal from MMCM
        independent_clock_bufg : in std_logic;                     -- 200 MHz independent clock
        resetdone              : out std_logic;
        cplllock               : out std_logic;                    -- The GT transceiver has completed its reset cycle
        mmcm_reset             : out std_logic;                    -- Use to divide the rxoutclk
        -- GMII Interface
        -----------------
        sgmii_clk_r            : out std_logic;                 
        sgmii_clk_f            : out std_logic;                 
        sgmii_clk_en           : out std_logic;                    -- Clock enable for client MAC
        gmii_txd               : in std_logic_vector(7 downto 0);  -- Transmit data from client MAC.
        gmii_tx_en             : in std_logic;                     -- Transmit control signal from client MAC.
        gmii_tx_er             : in std_logic;                     -- Transmit control signal from client MAC.
        gmii_rxd               : out std_logic_vector(7 downto 0); -- Received Data to client MAC.
        gmii_rx_dv             : out std_logic;                    -- Received control signal to client MAC.
        gmii_rx_er             : out std_logic;                    -- Received control signal to client MAC.
        gmii_isolate           : out std_logic;                    -- Tristate control to electrically isolate GMII.
        -- Configuration
        -----------------------------
        configuration_vector : in std_logic_vector(4 downto 0);  -- Alternative to MDIO interface.: [4]AnEnable,[3]IsolateDisabled,[2]PowerDownDisabled,[1]LoopbackDisabled,[0]UnidirectionalDisabled
        an_interrupt         : out std_logic;                    -- Interrupt to processor to signal that Auto-Negotiation has completed
        an_adv_config_vector : in std_logic_vector(15 downto 0); -- Alternate interface to program REG4 (AN ADV)
        an_restart_config    : in std_logic;                     -- Alternate signal to modify AN restart bit in REG0
        -- Speed Control
        ----------------
        speed_is_10_100      : in std_logic;                     -- Core should operate at either 10Mbps or 100Mbps speeds
        speed_is_100         : in std_logic;                      -- Core should operate at 100Mbps speed
        -- General IO
        --------------
        status_vector        : out std_logic_vector(15 downto 0); -- Core status.
        reset                : in std_logic;                     -- Asynchronous reset for entire core.
        signal_detect        : in std_logic;                      -- Input from PMD to indicate presence of optical input.
        -- GT common
        -------------
        gt0_qplloutclk_in     : in std_logic;
        gt0_qplloutrefclk_in  : in std_logic
      );
    end component;

	signal gmii_txd: std_logic_vector(7 downto 0);
	signal gmii_rxd: std_logic_vector(7 downto 0);
	signal gmii_tx_en: std_logic;
	signal gmii_tx_er: std_logic;
	signal gmii_rx_dv: std_logic;
	signal gmii_rx_er: std_logic;
	signal gmii_rx_clk: std_logic;
	signal clkin: std_logic;
	signal clk125: std_logic;
	signal txoutclk_ub: std_logic;
	signal txoutclk: std_logic;
    signal rxoutclk_ub: std_logic;
    signal rxoutclk: std_logic;
    signal clk125_ub: std_logic;
	signal clk_fr: std_logic;
	signal clk62_5_ub: std_logic;
	signal clk62_5: std_logic;
	signal clkfb: std_logic;
	signal rstn: std_logic;
	signal phy_done: std_logic;
	signal mmcm_locked: std_logic;
	signal mmcm_reset: std_logic;
	signal locked_int: std_logic;
	signal status: std_logic_vector(15 downto 0);


begin
	
	ibuf0: component IBUFDS_GTE2 
	   port map(
		i => gt_clkp,
		ib => gt_clkn,
		o => clkin,
		ceb => '0'
	   );
	
	bufg_fr: component BUFG 
	   port map(
		i => clkin,
		o => clk_fr
	   );
	
	clk125_fr <= clk_fr;

	bufg_tx: component BUFG 
	   port map(
		i => txoutclk_ub,
		o => txoutclk
	   );
	
	bufg_rx: component BUFG 
          port map(
           i => rxoutclk_ub,
           o => rxoutclk
          );


	mcmm: component MMCME2_BASE
		generic map(
			CLKIN1_PERIOD => 16.0,
			CLKFBOUT_MULT_F => 16.0,
			CLKOUT1_DIVIDE => 16,
			CLKOUT2_DIVIDE => 8)
		port map(
			clkin1 => txoutclk,
			clkout1 => clk62_5_ub,
			clkout2 => clk125_ub,
			clkfbout => clkfb,
			clkfbin => clkfb,
			rst => rsti,
			pwrdwn => '0',
			locked => mmcm_locked);
	
	bufr_125: BUFG
		port map(
			i => clk125_ub,
			o => clk125
		);

	clk125_out <= clk125;

	bufr_62_5: BUFG
		port map(
			i => clk62_5_ub,
			o => clk62_5
		);

	process(clk_fr)
	begin
		if rising_edge(clk_fr) then
			locked_int <= mmcm_locked and phy_done;
		end if;
	end process;

	locked <= locked_int;
	rstn <= not (not locked_int or rsti);


	mac: tri_mode_ethernet_mac_0
		port map(
			glbl_rstn => rstn,
			rx_axi_rstn => '1',
			tx_axi_rstn => '1',
			rx_reset => open,
			gtx_clk => clk125,
			rx_mac_aclk => open,
			clk_enable => '1',
			speedis100 => open,
            speedis10100 => open,
			rx_axis_mac_tdata => rx_data,
			rx_axis_mac_tvalid => rx_valid,
			rx_axis_mac_tlast => rx_last,
			rx_axis_mac_tuser => rx_error,
			--rx_axis_filter_tuser => open,
			rx_statistics_vector => open,
			rx_statistics_valid => open,
			tx_axis_mac_tdata => tx_data,
			tx_axis_mac_tvalid => tx_valid,
			tx_axis_mac_tlast => tx_last,
			tx_axis_mac_tuser(0) => tx_error,
			tx_axis_mac_tready => tx_ready,
			tx_ifg_delay => X"00",
			tx_statistics_vector => open,
			tx_statistics_valid => open,
			tx_mac_aclk => open, 
			tx_reset => open,
			pause_req => '0',
			pause_val => X"0000",
			gmii_txd => gmii_txd,
			gmii_tx_en => gmii_tx_en,
			gmii_tx_er => gmii_tx_er,
			gmii_rxd => gmii_rxd,
			gmii_rx_dv => gmii_rx_dv,
			gmii_rx_er => gmii_rx_er,
			rx_configuration_vector => X"0000_0000_0000_0000_0812",
			tx_configuration_vector => X"0000_0000_0000_0000_0012"
		);

	phy: gig_ethernet_pcs_pma_0
		port map(
			gtrefclk => clkin,
            gtrefclk_bufg => clk_fr,
            independent_clock_bufg => clk200,
            txp => gt_txp,
			txn => gt_txn,
			rxp => gt_rxp,
			rxn => gt_rxn,
			userclk => clk62_5,
            userclk2 => clk125,
			txoutclk => txoutclk_ub,
			rxoutclk => rxoutclk_ub,
			rxuserclk => rxoutclk, --maybe should use rxoutclk and generate bufg with MMCM and mmcm_reset
			rxuserclk2 => rxoutclk, --maybe should use rxoutclk and generate bufg with MMCM and mmcm_reset
			resetdone => phy_done,
			mmcm_locked => mmcm_locked,
			mmcm_reset => mmcm_reset,
			cplllock => open,
			pma_reset => rsti,
			gmii_txd => gmii_txd,
			gmii_tx_en => gmii_tx_en,
			gmii_tx_er => gmii_tx_er,
			gmii_rxd => gmii_rxd,
			gmii_rx_dv => gmii_rx_dv,
			gmii_rx_er => gmii_rx_er,
			gmii_isolate => open,
			configuration_vector => "10000",
			an_interrupt => open,
			an_adv_config_vector => "1000100000100001", 
			an_restart_config => '1',
			speed_is_10_100 => '0',
            speed_is_100 => '0', 
            status_vector => status,
			reset => rsti,
			signal_detect => '1',
		    gt0_qplloutclk_in => '0',
			gt0_qplloutrefclk_in => '0'
		);

end rtl;

