
set_property PACKAGE_PIN AD11 [get_ports SYSCLK_N] 
set_property PACKAGE_PIN AD12 [get_ports SYSCLK_P] 
set_property IOSTANDARD DIFF_SSTL15 [get_ports SYSCLK_P]
set_property IOSTANDARD DIFF_SSTL15 [get_ports SYSCLK_N]

#EXTERNAL CLOCK
#set_property PACKAGE_PIN AJ32 [get_ports i_EXT_CLK_P]
#set_property PACKAGE_PIN AK32 [get_ports i_EXT_CLK_N]
#set_property IOSTANDARD LVDS [get_ports i_EXT_CLK_P]
#set_property IOSTANDARD LVDS [get_ports i_EXT_CLK_N]
#create_clock -period 1000.000 -name i_EXT_CLK_P -waveform {0.000 500.000} [get_ports i_EXT_CLK_P]
set_property VCCAUX_IO DONTCARE [get_ports SYSCLK_P]


# sma clock
###set_property PACKAGE_PIN L25 [get_ports i_USER_SMA_CLOCK_P]
###set_property PACKAGE_PIN K25 [get_ports i_USER_SMA_CLOCK_N]
###set_property IOSTANDARD LVDS_25 [get_ports i_USER_SMA_CLOCK_N]
###set_property IOSTANDARD LVDS_25 [get_ports i_USER_SMA_CLOCK_P]


#######################################################################################################################################################
#######################################################################################################################################################

#Ethernet PHY pin K3
set_property PACKAGE_PIN L20 [get_ports PHY_RESET] 
set_property IOSTANDARD LVCMOS18 [get_ports PHY_RESET]
#Ethernet PHY pin A3
#Ethernet PHY pin A4
#Ethernet PHY pin A7
#Ethernet PHY pin A8
set_property PACKAGE_PIN H5 [get_ports SGMII_RX_N] 
set_property PACKAGE_PIN H6 [get_ports SGMII_RX_P] 
set_property PACKAGE_PIN J4 [get_ports SGMII_TX_P] 
set_property PACKAGE_PIN J3 [get_ports SGMII_TX_N] 
#Ethernet PHY pin U2_6
#Ethernet PHY pin U2_7
set_property PACKAGE_PIN G8 [get_ports SGMIICLK_Q0_P] 
set_property PACKAGE_PIN G7 [get_ports SGMIICLK_Q0_N] 



#### below froim Virtex
#set_property PACKAGE_PIN E18 [get_ports SYSCLK_N]
#set_property PACKAGE_PIN E19 [get_ports SYSCLK_P]
#set_property IOSTANDARD DIFF_SSTL15 [get_ports SYSCLK_P]
#set_property IOSTANDARD DIFF_SSTL15 [get_ports SYSCLK_N]

##EXTERNAL CLOCK
##set_property PACKAGE_PIN AJ32 [get_ports i_EXT_CLK_P]
##set_property PACKAGE_PIN AK32 [get_ports i_EXT_CLK_N]
##set_property IOSTANDARD LVDS [get_ports i_EXT_CLK_P]
##set_property IOSTANDARD LVDS [get_ports i_EXT_CLK_N]
##create_clock -period 1000.000 -name i_EXT_CLK_P -waveform {0.000 500.000} [get_ports i_EXT_CLK_P]
#set_property VCCAUX_IO DONTCARE [get_ports SYSCLK_P]


## sma clock
####set_property PACKAGE_PIN L25 [get_ports i_USER_SMA_CLOCK_P]
####set_property PACKAGE_PIN K25 [get_ports i_USER_SMA_CLOCK_N]
####set_property IOSTANDARD LVDS_25 [get_ports i_USER_SMA_CLOCK_N]
####set_property IOSTANDARD LVDS_25 [get_ports i_USER_SMA_CLOCK_P]


########################################################################################################################################################
########################################################################################################################################################

##Ethernet PHY pin K3
#set_property PACKAGE_PIN AJ33 [get_ports PHY_RESET]
#set_property IOSTANDARD LVCMOS18 [get_ports PHY_RESET]
##Ethernet PHY pin A3
##Ethernet PHY pin A4
##Ethernet PHY pin A7
##Ethernet PHY pin A8
#set_property PACKAGE_PIN AM7 [get_ports SGMII_RX_N]
#set_property PACKAGE_PIN AM8 [get_ports SGMII_RX_P]
#set_property PACKAGE_PIN AN2 [get_ports SGMII_TX_P]
#set_property PACKAGE_PIN AN1 [get_ports SGMII_TX_N]
##Ethernet PHY pin U2_6
##Ethernet PHY pin U2_7
#set_property PACKAGE_PIN AH8 [get_ports SGMIICLK_Q0_P]
#set_property PACKAGE_PIN AH7 [get_ports SGMIICLK_Q0_N]




