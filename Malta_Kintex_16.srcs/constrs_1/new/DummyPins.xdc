


#used to send trigger out (this is FMC1_HPC_HA23_P)
#set_property PACKAGE_PIN A36 [get_ports o_trigger_P]
#set_property PACKAGE_PIN A35 [get_ports o_trigger_N]
#set_property IOSTANDARD LVDS25 [get_ports o_trigger_P]
#set_property IOSTANDARD LVDS25 [get_ports o_trigger_N]

#UNCOMMENT FOR MALTA CARRIER V2 bob
#set_property PACKAGE_PIN T31 [get_ports {o_FMC_LED_N[1]}]
#set_property PACKAGE_PIN U31 [get_ports {o_FMC_LED_P[1]}]

#UNCOMMENT FOR MALTA CARRIER V3 bob
#set_property PACKAGE_PIN N21 [get_ports {o_FMC_LED_N[1]}]
#set_property PACKAGE_PIN P21 [get_ports {o_FMC_LED_P[1]}]

#this was used to light up the LED on the second board .... now some pin used to control the sent pattern
set_property PACKAGE_PIN C21 [get_ports {o_FMC_LED_N[0]}]
set_property PACKAGE_PIN D21 [get_ports {o_FMC_LED_P[0]}]
set_property PACKAGE_PIN C22 [get_ports {o_FMC_LED_N[2]}]
set_property PACKAGE_PIN D22 [get_ports {o_FMC_LED_P[2]}]
set_property PACKAGE_PIN F22 [get_ports {o_FMC_LED_N[3]}]
set_property PACKAGE_PIN G22 [get_ports {o_FMC_LED_P[3]}]
set_property PACKAGE_PIN C16 [get_ports {o_FMC_LED_N[4]}]
set_property PACKAGE_PIN D16 [get_ports {o_FMC_LED_P[4]}]
set_property PACKAGE_PIN C17 [get_ports {o_FMC_LED_P[5]}]
set_property PACKAGE_PIN B17 [get_ports {o_FMC_LED_N[5]}]
set_property PACKAGE_PIN A16 [get_ports {o_FMC_LED_P[6]}]
set_property PACKAGE_PIN A17 [get_ports {o_FMC_LED_N[6]}]
set_property PACKAGE_PIN F17 [get_ports {o_FMC_LED_N[7]}]
set_property PACKAGE_PIN G17 [get_ports {o_FMC_LED_P[7]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_N[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_P[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_N[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_P[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_N[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_P[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_N[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_P[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_N[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_P[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_N[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_P[7]}]
set_property IOSTANDARD LVDS_25 [get_ports {o_FMC_LED_N[7]}]












#### below froim Virtex

##used to send trigger out (this is FMC1_HPC_HA23_P)
##set_property PACKAGE_PIN A36 [get_ports o_trigger_P]
##set_property PACKAGE_PIN A35 [get_ports o_trigger_N]
##set_property IOSTANDARD LVDS [get_ports o_trigger_P]
##set_property IOSTANDARD LVDS [get_ports o_trigger_N]

##UNCOMMENT FOR MALTA CARRIER V2 bob
##set_property PACKAGE_PIN T31 [get_ports {o_FMC_LED_N[1]}]
##set_property PACKAGE_PIN U31 [get_ports {o_FMC_LED_P[1]}]

##UNCOMMENT FOR MALTA CARRIER V3 bob
#set_property PACKAGE_PIN N21 [get_ports {o_FMC_LED_N[1]}]
#set_property PACKAGE_PIN P21 [get_ports {o_FMC_LED_P[1]}]

##this was used to light up the LED on the second board .... now some pin used to control the sent pattern
#set_property PACKAGE_PIN U29 [get_ports {o_FMC_LED_N[0]}]
#set_property PACKAGE_PIN V29 [get_ports {o_FMC_LED_P[0]}]

#set_property PACKAGE_PIN V31 [get_ports {o_FMC_LED_N[2]}]
#set_property PACKAGE_PIN V30 [get_ports {o_FMC_LED_P[2]}]
#set_property PACKAGE_PIN M29 [get_ports {o_FMC_LED_N[3]}]
#set_property PACKAGE_PIN M28 [get_ports {o_FMC_LED_P[3]}]
#set_property PACKAGE_PIN L30 [get_ports {o_FMC_LED_N[4]}]
#set_property PACKAGE_PIN L29 [get_ports {o_FMC_LED_P[4]}]
#set_property PACKAGE_PIN T29 [get_ports {o_FMC_LED_P[5]}]
#set_property PACKAGE_PIN T30 [get_ports {o_FMC_LED_N[5]}]
#set_property PACKAGE_PIN R30 [get_ports {o_FMC_LED_P[6]}]
#set_property PACKAGE_PIN P31 [get_ports {o_FMC_LED_N[6]}]
#set_property PACKAGE_PIN K30 [get_ports {o_FMC_LED_N[7]}]
#set_property PACKAGE_PIN K29 [get_ports {o_FMC_LED_P[7]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_P[0]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_N[0]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_P[1]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_N[1]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_P[2]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_N[2]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_P[3]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_N[3]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_P[4]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_N[4]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_P[5]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_N[5]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_P[6]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_N[6]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_P[7]}]
#set_property IOSTANDARD LVDS [get_ports {o_FMC_LED_N[7]}]












